const jwt = require('jsonwebtoken');
require('dotenv').config();

module.exports={
    validateAddUser:validateAddUser,
    verifytoken:verifytoken
}
const secretKey= process.env.secretKey;
function validateAddUser(req,res,next){
    const { fullName, email, password } = req.body;
    if (!fullName || !email || !password) {
        return res.status(400).json({ error: 'fullName,email and password are required fields.' });
      }
    next();
}
function verifytoken (req,res,nxt){
    const bearerHeader = req.headers['Authorization'];
    if(!(typeof bearerHeader ==='undefined')){
        const bearer = bearerHeader.split(" ");
        const token = bearer[1];
        jwt.verify(token,secretKey,(err,authData)=>{
            if(err){
                res.send({result:"token is not valid"});
            }else{
                req.authData=authData;
                next();
            }
        })

        
    }else{
        res.send({result:"token is not valid"});
    }
}