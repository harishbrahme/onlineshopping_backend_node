const express = require('express');
const router = express.Router();
const userController = require('../controllers/user/user.controller');
const userInterceptor = require('../interceptor/user.interceptor')

router.post('/addUser',userInterceptor.validateAddUser,userController.addUser);
router.post('/loginuser',userController.loginUser)
module.exports = router;