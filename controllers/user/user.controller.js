const User = require('./user.model');
const jwt = require('jsonwebtoken');
require('dotenv').config();
module.exports = {
    addUser: addUser,
    loginUser:loginUser
}
const secretKey= process.env.secretKey;

function addUser(req, res) {
    const { email } = req.body;
    const userDetails = req.body;
    serchUser(email, (err, user) => {
        if (err) throw err;

        if (!user) {
            let userData = new User(userDetails);
            let newUser = userData.save().catch((err) => {
                throw err;
            })
            res.status(200).json(newUser)
        }else{
            res.status(200).send("this emailId has been already exite ");
        }

    })
}

function loginUser(req, res) {
    const { email } = req.body;
    const userDetails = req.body;
    serchUser(email, (err, user) => {
        if (err) throw err;

        if (!user) {
            res.status(200).send("user doesn't exite ");
        }else{
            jwt.sign({user},secretKey,{expiresIn:'300s'},(err,token)=>{
                if(err){
                    res.status(503).json("something went wrong")
                }else{
                    user.token=token
                    res.status(200).json({result:"user login successfuly",user});
                }
            })
        }
    })
}


async function serchUser(userMailId, cb) {
    try {
        let user = await User.findOne({ email: userMailId });
        cb(null, user)
    } catch (err) {
        cb(err, null)
    }
}