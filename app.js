const express = require("express");
const app = express();
var con = require("./connection");
var bodyparser = require('body-parser');
const multer = require('multer');
const indexRouter =require('./routes/index')
const storage = multer.memoryStorage();
const upload = multer({ storage: storage });
const mongoose = require("mongoose");
const cors = require("cors");
require('dotenv').config();

app.use(cors());
const PORT=process.env.PORT;
console.log(PORT);
app.listen(PORT,() => {
  console.log(`server is run on ${PORT} port`);
});




app.use(bodyparser.json());
app.use(bodyparser.urlencoded({extended:true}));

let connectionString = process.env.MONGO_URI;
console.log(connectionString,"connectionString");
mongoose.connect(connectionString, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  maxIdleTimeMS: 120000,
  serverSelectionTimeoutMS: 80000,
  socketTimeoutMS: 0,
  connectTimeoutMS: 0
}).catch((err) => {
  console.log(err);
});
//Get the default connection
let dbConnect = mongoose.connection;

dbConnect.on("connected", () => {
  // logger.debug("Database Connected.");
  console.log("Database Connected.");
});
//Bind connection to error event (to get notification of connection errors)
dbConnect.on("error", (error) => {
 console.log("Error Database connection");
 console.log(error);
});

// app.get('/getAllProducts', (req, res) => {
//     con.query('SELECT * FROM product', (err, result) => {
//       if (err) {
//         console.error('Error querying data from MySQL:', err);
//         res.status(500).json({ error: 'Internal Server Error' });
//         return;
//       }
  
//       console.log(result);
//       res.status(200).json(result);
//     });
//   });
  
  // app.listen(PORT, () => {
  //   console.log(`Server is listening at ${PORT}`);
  // });
app.use(upload.any());
// const upload = multer({
//     storage:multer.diskStorage({
//         destination:function(req,file,cb){
//             cb(null,"/")
//         },
//         filename:function(req,file,cb){
//             console.log
//             cb(null,file.fieldname+"-"+Date.now()+".jpg")
//         }
//     })
// }).single("image")
// app.post("/addProduct", (req, res) => {
//     const { name, price, description } = req.body;
//     const imageBuffer = req.files[0].buffer; // Assuming only one file is uploaded

//     con.connect(function (err) {
//         if (err) {
//             console.error('Error connecting to MySQL:', err);
//             res.status(500).json({ error: 'Internal Server Error' });
//             return;
//         }

//         // Use a parameterized query to avoid SQL injection
//         const query = "INSERT INTO product (name, price, description, image) VALUES (?, ?, ?, ?)";

//         con.query(query, [name, price, description, imageBuffer], (err, result) => {
//             if (err) {
//                 console.error('Error inserting data into MySQL:', err);
//                 res.status(500).json({ error: 'Internal Server Error' });
//                 return;
//             }

//             console.log('Data inserted successfully');
//             res.status(200).json(result);
//         });
//     });
// });
// app.get("/",(req,res)=>{
//     return res.send("Welcome to online shopping platform harish-pranita test");
// })
app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader("Access-Control-Allow-Methods", "PUT,GET,POST,PATCH");
  res.setHeader(
    "Access-Control-Allow-Headers",
    "X-Requested-With,content-type,Origin,Accept,Authorization"
  );
  res.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
  res.setHeader("Pragma", "no-cache");
  res.setHeader("Expires", "0");
  next();
});
app.use('/api', indexRouter);
app.use((req,res)=>{
    res.status(404).json({ error: 'Resource not found' });
})
